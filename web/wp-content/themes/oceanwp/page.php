<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package OceanWP WordPress theme
 */

get_header(); ?>
<!-- Appelle le fichier d'en-tête de votre thème -->

<?php do_action('ocean_before_content_wrap'); ?>
<!-- Point d'ancrage pour ajouter du contenu avant la balise content-wrap -->

<div id="content-wrap" class="container clr">
	<!-- Conteneur principal du contenu -->

	<?php do_action('ocean_before_primary'); ?>
	<!-- Point d'ancrage pour ajouter du contenu avant la balise primary -->

	<div id="primary" class="content-area clr">
		<!-- Balise principale de contenu -->

		<?php do_action('ocean_before_content'); ?>
		<!-- Point d'ancrage pour ajouter du contenu avant la balise content -->

		<div id="content" class="site-content clr">
			<!-- Contenu principal de la page -->

			<?php do_action('ocean_before_content_inner'); ?>
			<!-- Point d'ancrage pour ajouter du contenu avant la balise content-inner -->

			<?php
			// Elementor `single` location.
			// Vérifie si Elementor est actif et s'il y a un modèle Elementor pour cette page
			if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('single')) {

				// Commence la boucle WordPress pour afficher le contenu de la page
				while (have_posts()) :
					the_post();

					// Inclut le modèle de mise en page de la page
					get_template_part('partials/page/layout');

				endwhile;
			}
			?>

			<?php do_action('ocean_after_content_inner'); ?>
			<!-- Point d'ancrage pour ajouter du contenu après la balise content-inner -->

		</div><!-- #content -->
		<!-- Ferme la balise content -->

		<?php do_action('ocean_after_content'); ?>
		<!-- Point d'ancrage pour ajouter du contenu après la balise content -->

	</div><!-- #primary -->
	<!-- Ferme la balise primary -->

	<?php do_action('ocean_after_primary'); ?>
	<!-- Point d'ancrage pour ajouter du contenu après la balise primary -->

</div><!-- #content-wrap -->
<!-- Ferme la balise content-wrap -->

<?php do_action('ocean_after_content_wrap'); ?>
<!-- Point d'ancrage pour ajouter du contenu après la balise content-wrap -->

<?php get_footer(); ?>
<!-- Appelle le fichier de pied de page de votre thème -->