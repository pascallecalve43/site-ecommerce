<?php

/**
 * The template for displaying offline pages in PWA.
 * Ce modèle est utilisé pour afficher les pages hors ligne dans une application web progressive (PWA).
 *
 * @package OceanWP WordPress theme
 * @since   1.8.8
 */

// Appelle l'en-tête spécifique à PWA
pwa_get_header('pwa');

// Exécute l'action pour afficher le contenu hors ligne
do_action('ocean_do_offline');

// Appelle le pied de page spécifique à PWA
pwa_get_footer('pwa');
