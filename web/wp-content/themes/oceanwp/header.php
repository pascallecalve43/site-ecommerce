<?php

/**
 * The Header for our theme.
 * Ce fichier est l'en-tête de notre thème.
 *
 * @package OceanWP WordPress theme
 */

?>
<!DOCTYPE html>
<!-- Déclaration du type de document -->

<html class="<?php echo esc_attr(oceanwp_html_classes()); ?>" <?php language_attributes(); ?>>
<!-- Balise d'ouverture de la balise html avec des classes et attributs de langue dynamiques -->

<head>
	<!-- Balise d'en-tête -->
	<meta charset="<?php bloginfo('charset'); ?>">
	<!-- Définit l'encodage des caractères en fonction des paramètres du thème -->

	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- Liens vers le profil XFN (XHTML Friends Network) -->

	<?php wp_head(); ?>
	<!-- Appelle les scripts, les styles et autres éléments de l'en-tête WordPress -->

</head>

<body <?php body_class(); ?> <?php oceanwp_schema_markup('html'); ?>>
	<!-- Balise body avec des classes de contenu dynamiques et des attributs de schéma -->

	<?php wp_body_open(); ?>
	<!-- Point d'ancrage pour ajouter du contenu avant la balise body -->

	<?php do_action('ocean_before_outer_wrap'); ?>
	<!-- Point d'ancrage pour ajouter du contenu avant la balise outer-wrap -->

	<div id="outer-wrap" class="site clr">
		<!-- Conteneur principal du site -->

		<a class="skip-link screen-reader-text" href="#main"><?php echo esc_html(oceanwp_theme_strings('owp-string-header-skip-link', false)); ?></a>
		<!-- Lien de saut pour les lecteurs d'écran -->

		<?php do_action('ocean_before_wrap'); ?>
		<!-- Point d'ancrage pour ajouter du contenu avant la balise wrap -->

		<div id="wrap" class="clr">
			<!-- Conteneur du contenu principal -->

			<?php do_action('ocean_top_bar'); ?>
			<!-- Point d'ancrage pour ajouter du contenu avant la barre supérieure -->

			<?php do_action('ocean_header'); ?>
			<!-- Point d'ancrage pour ajouter du contenu avant l'en-tête -->

			<?php do_action('ocean_before_main'); ?>
			<!-- Point d'ancrage pour ajouter du contenu avant la balise main -->

			<main id="main" class="site-main clr" <?php oceanwp_schema_markup('main'); ?> role="main">
				<!-- Balise principale de contenu -->

				<?php do_action('ocean_page_header'); ?>
				<!-- Point d'ancrage pour ajouter du contenu avant l'en-tête de la page -->