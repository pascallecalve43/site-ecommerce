<?php

/**
 * Template Name: Landing Page
 * Définit le nom du modèle de page comme "Landing Page".
 * @package OceanWP WordPress theme
 */
?>

<!DOCTYPE html>
<html class="<?php echo esc_attr(oceanwp_html_classes()); ?>" <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<!-- Ajoute la balise meta pour définir l'encodage de caractères -->
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<!-- Ajoute les éléments nécessaires dans la section head, tels que les styles CSS et les scripts JavaScript -->
</head>

<!-- Begin Body -->

<body <?php body_class(); ?> <?php oceanwp_schema_markup('html'); ?>>
	<!-- Ajoute des classes au corps de la page, utiles pour le stylage CSS -->

	<?php wp_body_open(); ?>
	<!-- Ajoute des actions pour le contenu avant et après le corps de la page -->

	<?php do_action('ocean_before_outer_wrap'); ?>
	<!-- Point d'ancrage pour ajouter du contenu avant la balise outer-wrap -->

	<div id="outer-wrap" class="site clr">

		<a class="skip-link screen-reader-text" href="#main"><?php echo esc_html(oceanwp_theme_strings('owp-string-header-skip-link', false)); ?></a>
		<!-- Ajoute un lien de saut pour les lecteurs d'écran -->

		<?php do_action('ocean_before_wrap'); ?>
		<!-- Point d'ancrage pour ajouter du contenu avant la balise wrap -->

		<div id="wrap" class="clr">

			<?php do_action('ocean_before_main'); ?>
			<!-- Point d'ancrage pour ajouter du contenu avant la balise main -->

			<main id="main" class="site-main clr" <?php oceanwp_schema_markup('main'); ?> role="main">
				<!-- Ajoute des attributs au contenu principal -->

				<?php do_action('ocean_before_content_wrap'); ?>
				<!-- Point d'ancrage pour ajouter du contenu avant la balise content-wrap -->

				<div id="content-wrap" class="container clr">

					<?php do_action('ocean_before_primary'); ?>
					<!-- Point d'ancrage pour ajouter du contenu avant la balise primary -->

					<section id="primary" class="content-area clr">
						<!-- Ajoute des classes et un ID à la section principale -->

						<?php do_action('ocean_before_content'); ?>
						<!-- Point d'ancrage pour ajouter du contenu avant la balise content -->

						<div id="content" class="site-content clr">
							<!-- Ajoute des classes à la section de contenu -->

							<?php do_action('ocean_before_content_inner'); ?>
							<!-- Point d'ancrage pour ajouter du contenu avant la balise content-inner -->

							<?php
							while (have_posts()) :
								the_post();
							?>
								<!-- Boucle WordPress pour afficher le contenu de la page -->
								<div class="entry-content entry clr">
									<?php the_content(); ?>
									<!-- Affiche le contenu de la page -->
								</div><!-- .entry-content -->

							<?php endwhile; ?>

							<?php do_action('ocean_after_content_inner'); ?>
							<!-- Point d'ancrage pour ajouter du contenu après la balise content-inner -->

						</div><!-- #content -->

						<?php do_action('ocean_after_content'); ?>
						<!-- Point d'ancrage pour ajouter du contenu après la balise content -->

					</section><!-- #primary -->

					<?php do_action('ocean_after_primary'); ?>
					<!-- Point d'ancrage pour ajouter du contenu après la balise primary -->

				</div><!-- #content-wrap -->
				<!-- Ferme la balise content-wrap -->

				<?php do_action('ocean_after_content_wrap'); ?>
				<!-- Point d'ancrage pour ajouter du contenu après la balise content-wrap -->

			</main><!-- #main-content -->
			<!-- Ferme la balise main -->

			<?php do_action('ocean_after_main'); ?>
			<!-- Point d'ancrage pour ajouter du contenu après la balise main -->

		</div><!-- #wrap -->
		<!-- Ferme la balise wrap -->

		<?php do_action('ocean_after_wrap'); ?>
		<!-- Point d'ancrage pour ajouter du contenu après la balise wrap -->

	</div><!-- .outer-wrap -->
	<!-- Ferme la balise outer-wrap -->

	<?php do_action('ocean_after_outer_wrap'); ?>
	<!-- Point d'ancrage pour ajouter du contenu après la balise outer-wrap -->

	<?php wp_footer(); ?>
	<!-- Ajoute les scripts JavaScript et d'autres éléments nécessaires juste avant la fermeture de la balise body -->
</body>

</html>