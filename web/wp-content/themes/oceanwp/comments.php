<?php

/**
 * Le modèle pour afficher les commentaires.
 *
 * Ce fichier gère l'affichage des commentaires sur les articles ou les pages WordPress.
 * L'affichage réel des commentaires est géré par une fonction de rappel située dans functions/comments-callback.php
 *
 * @package Thème WordPress OceanWP
 */

// Retourne si un mot de passe est requis.
if (post_password_required()) {
	return; // Si un mot de passe est nécessaire pour accéder à la publication, le script s'arrête ici.
}

// Ajoute des classes à l'enveloppe principale des commentaires.
$classes = 'comments-area clr';

if (get_comments_number() !== 0) {
	$classes .= ' has-comments'; // Ajoute la classe 'has-comments' si des commentaires sont présents.
}

if (!comments_open() && get_comments_number() < 1) {
	$classes .= ' empty-closed-comments';
	return; // Si les commentaires sont fermés et qu'il n'y a pas de commentaires, le script s'arrête ici.
}

if ('full-screen' === oceanwp_post_layout()) {
	$classes .= ' container'; // Ajoute la classe 'container' si la mise en page est en plein écran.
}

// Récupère la position du formulaire de commentaire.
$comment_position = apply_filters('ocean_comment_form_position', get_theme_mod('ocean_comment_form_position'));
$comment_position = $comment_position ? $comment_position : 'after'; // Définit la position par défaut du formulaire de commentaire.

// Arguments du formulaire de commentaire.
$args = array(
	/* Traduction : 1: connecté pour commenter */
	'must_log_in'          => '<p class="must-log-in">' . sprintf(esc_html__('Vous devez être %1$sconnecté%2$s pour poster un commentaire.', 'oceanwp'), '<a href="' . wp_login_url(apply_filters('the_permalink', get_permalink())) . '">', '</a>') . '</p>',
	'logged_in_as'         => '<p class="logged-in-as">' . esc_html__('Connecté en tant que', 'oceanwp') . ' <a href="' . admin_url('profile.php') . '">' . $user_identity . '</a>.<span class="screen-reader-text">' . esc_html(oceanwp_theme_strings('owp-string-comment-profile-edit', false)) . '</span> <a href="' . wp_logout_url(get_permalink()) . '" aria-label="' . esc_attr(oceanwp_theme_strings('owp-string-comment-logout-text', false)) . '">' . esc_html__('Déconnexion &raquo;', 'oceanwp') . '</a></p>',
	'comment_notes_before' => false,
	'comment_notes_after'  => false,
	'comment_field'        => '<div class="comment-textarea"><label for="comment" class="screen-reader-text">' . esc_html__('Commentaire', 'oceanwp') . '</label><textarea name="comment" id="comment" cols="39" rows="4" tabindex="0" class="textarea-comment" placeholder="' . esc_attr(oceanwp_theme_strings('owp-string-comment-placeholder', false)) . '"></textarea></div>',
	'id_submit'            => 'comment-submit',
	'label_submit'         => esc_html(oceanwp_theme_strings('owp-string-comment-post-button', false)),
);

?>

<section id="comments" class="<?php echo esc_attr($classes); ?>">

	<?php
	// Affiche le formulaire de commentaire si la position est définie avant la liste des commentaires.
	if ('before' === $comment_position) {
		comment_form($args); // Affiche le formulaire de commentaire avant la liste des commentaires.
	}
	?>

	<?php

	if (have_comments()) :

		// Récupère le titre des commentaires.
		$comments_number = number_format_i18n(get_comments_number());
		if ('1' === $comments_number) {
			$comments_title = esc_html__('Cet article a un commentaire', 'oceanwp');
		} else {
			/* Traduction : %s: Nombre de commentaires */
			$comments_title = sprintf(esc_html__('Cet article a %s commentaires', 'oceanwp'), $comments_number);
		}
		$comments_title = apply_filters('ocean_comments_title', $comments_title);

	?>

		<h3 class="theme-heading comments-title">
			<span class="text"><?php echo esc_html($comments_title); ?></span>
		</h3>

		<ol class="comment-list">
			<?php
			// Liste des commentaires.
			wp_list_comments(
				array(
					'callback' => 'oceanwp_comment', // Appelle la fonction de rappel 'oceanwp_comment' pour afficher chaque commentaire.
					'style'    => 'ol',
					'format'   => 'html5',
				)
			);
			?>
		</ol><!-- .comment-list -->

		<?php
		// Affiche la navigation des commentaires.
		if (function_exists('the_comments_navigation')) :

			$prev_comm_txt = is_rtl() ? oceanwp_icon('angle_right', false) : oceanwp_icon('angle_left', false);
			$next_comm_txt = is_rtl() ? oceanwp_icon('angle_left', false) : oceanwp_icon('angle_right', false);

			the_comments_navigation(
				array(
					'prev_text' => $prev_comm_txt . '<span class="screen-reader-text">' . esc_html__('Commentaire précédent', 'oceanwp') . '</span>' . esc_html__('Précédent', 'oceanwp'),
					'next_text' => esc_html__('Suivant', 'oceanwp') . $next_comm_txt . '</i><span class="screen-reader-text">' . esc_html__('Commentaire suivant', 'oceanwp') . '</span>',
				)
			);

		elseif (get_comment_pages_count() > 1 && get_option('page_comments')) :

		?>

			<div class="comment-navigation clr">
				<?php
				// Pagination des commentaires.
				paginate_comments_links(
					array(
						'prev_text' => $prev_comm_txt . '</i><span class="screen-reader-text">' . esc_html__('Commentaire précédent', 'oceanwp') . '</span>' . esc_html__('Précédent', 'oceanwp'),
						'next_text' => esc_html__('Suivant', 'oceanwp') . $next_comm_txt . '<span class="screen-reader-text">' . esc_html__('Commentaire suivant', 'oceanwp') . '</span>',
					)
				);
				?>
			</div>

		<?php endif; ?>

		<?php
		// Affiche un message si les commentaires sont fermés.
		if (!comments_open() && get_comments_number()) :
		?>
			<p class="no-comments"><?php esc_html_e('Les commentaires sont fermés.', 'oceanwp'); ?></p>
		<?php endif; ?>

	<?php endif; // Fin de la vérification have_comments(). 
	?>

	<?php
	// Affiche le formulaire de commentaire si la position est définie après la liste des commentaires (paramètre par défaut).
	if ('after' === $comment_position) {
		comment_form($args); // Affiche le formulaire de commentaire après la liste des commentaires.
	}
	?>

</section><!-- #comments -->