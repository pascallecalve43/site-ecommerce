<?php

/**
 * The template for displaying the footer.
 * Ce modèle affiche le pied de page du thème.
 *
 * @package OceanWP WordPress theme
 */

?>

</main><!-- #main -->

<?php do_action('ocean_after_main'); ?>

<?php do_action('ocean_before_footer'); ?>

<?php
// Elementor `footer` location.
// Vérifie si un emplacement de footer Elementor est utilisé, sinon affiche le footer par défaut.
if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('footer')) {
?>

	<?php do_action('ocean_footer'); ?>

<?php } ?>

<?php do_action('ocean_after_footer'); ?>

</div><!-- #wrap -->

<?php do_action('ocean_after_wrap'); ?>

</div><!-- #outer-wrap -->

<?php do_action('ocean_after_outer_wrap'); ?>

<?php
// If is not sticky footer.
// Vérifie si le footer n'est pas en mode sticky (collant), si c'est le cas, affiche le bouton de retour en haut de page.
if (!class_exists('Ocean_Sticky_Footer')) {
	get_template_part('partials/scroll-top');
}
?>

<?php
// Search overlay style.
// Si le style de recherche est en mode "overlay", inclut le modèle de recherche en overlay.
if ('overlay' === oceanwp_menu_search_style()) {
	get_template_part('partials/header/search-overlay');
}
?>

<?php
// If sidebar mobile menu style.
// Si le style de menu mobile est en mode "sidebar".
if ('sidebar' === oceanwp_mobile_menu_style()) {

	// Mobile panel close button.
	// Affiche le bouton de fermeture du menu mobile si activé dans les réglages.
	if (get_theme_mod('ocean_mobile_menu_close_btn', true)) {
		get_template_part('partials/mobile/mobile-sidr-close');
	}
?>

	<?php
	// Mobile Menu (if defined).
	// Affiche le menu mobile si défini.
	get_template_part('partials/mobile/mobile-nav');
	?>

<?php
	// Mobile search form.
	// Affiche le formulaire de recherche mobile si activé dans les réglages.
	if (get_theme_mod('ocean_mobile_menu_search', true)) {
		ob_start();
		get_template_part('partials/mobile/mobile-search');
		echo ob_get_clean();
	}
}
?>

<?php
// If full screen mobile menu style.
// Si le style de menu mobile est en mode "fullscreen", inclut le modèle de menu mobile en plein écran.
if ('fullscreen' === oceanwp_mobile_menu_style()) {
	get_template_part('partials/mobile/mobile-fullscreen');
}
?>

<?php wp_footer(); ?>
</body>

</html>