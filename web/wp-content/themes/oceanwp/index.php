<?php

/**
 * The main template file.
 * Ce fichier est le modèle le plus générique dans un thème WordPress et l'un des deux fichiers requis pour un thème (l'autre étant style.css).
 * Il est utilisé pour afficher une page lorsque rien de plus spécifique ne correspond à une requête.
 * Par exemple, il assemble la page d'accueil lorsqu'aucun fichier home.php n'existe.
 *
 * En savoir plus : http://codex.wordpress.org/Template_Hierarchy
 *
 * @package OceanWP WordPress theme
 */

get_header(); ?>
<!-- Appelle l'en-tête de votre thème -->

<?php do_action('ocean_before_content_wrap'); ?>
<!-- Point d'ancrage pour ajouter du contenu avant la balise content-wrap -->

<div id="content-wrap" class="container clr">
	<!-- Conteneur principal du contenu -->

	<?php do_action('ocean_before_primary'); ?>
	<!-- Point d'ancrage pour ajouter du contenu avant la balise primary -->

	<div id="primary" class="content-area clr">
		<!-- Balise principale de contenu -->

		<?php do_action('ocean_before_content'); ?>
		<!-- Point d'ancrage pour ajouter du contenu avant la balise content -->

		<div id="content" class="site-content clr">
			<!-- Contenu principal de la page -->

			<?php do_action('ocean_before_content_inner'); ?>
			<!-- Point d'ancrage pour ajouter du contenu avant la balise content-inner -->

			<?php
			// Vérifie si des articles existent
			if (have_posts()) :

				// Vérifie s'il s'agit d'un emplacement d'archive Elementor
				if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('archive')) {

					// Ajoute la prise en charge des pages d'archive EDD (Easy Digital Downloads)
					if (is_post_type_archive('download') || is_tax(array('download_category', 'download_tag'))) {

						do_action('ocean_before_archive_download');
			?>

						<div class="oceanwp-row <?php echo esc_attr(oceanwp_edd_loop_classes()); ?>">
							<?php
							// Compteur d'archive pour réinitialiser le flottement
							$oceanwp_count = 0;
							while (have_posts()) :
								the_post();
								$oceanwp_count++;
								get_template_part('partials/edd/archive');
								if (oceanwp_edd_entry_columns() === $oceanwp_count) {
									$oceanwp_count = 0;
								}
							endwhile;
							?>
						</div>

					<?php
						do_action('ocean_after_archive_download');
					} else {
					?>
						<div id="blog-entries" class="<?php oceanwp_blog_wrap_classes(); ?>">

							<?php
							// Définit le compteur pour réinitialiser le flottement
							$oceanwp_count = 0;
							?>

							<?php
							// Boucle à travers les articles
							while (have_posts()) :
								the_post();
							?>

								<?php
								// Ajoute au compteur
								$oceanwp_count++;
								?>

								<?php
								// Obtenir le contenu de l'entrée de l'article
								get_template_part('partials/entry/layout', get_post_type());
								?>

								<?php
								// Réinitialise le compteur pour effacer les flotteurs
								if (oceanwp_blog_entry_columns() === $oceanwp_count) {
									$oceanwp_count = 0;
								}
								?>

							<?php endwhile; ?>

						</div><!-- #blog-entries -->

				<?php
						// Affiche la pagination des articles
						oceanwp_blog_pagination();
					}
				}
				?>

			<?php
			// Aucun article trouvé
			else :
			?>

				<?php
				// Affiche un message si aucun article n'est trouvé
				get_template_part('partials/none');
				?>

			<?php endif; ?>

			<?php do_action('ocean_after_content_inner'); ?>

		</div><!-- #content -->
		<!-- Ferme la balise content -->

		<?php do_action('ocean_after_content'); ?>
		<!-- Point d'ancrage pour ajouter du contenu après la balise content -->

	</div><!-- #primary -->
	<!-- Ferme la balise primary -->

	<?php do_action('ocean_after_primary'); ?>
	<!-- Point d'ancrage pour ajouter du contenu après la balise primary -->

</div><!-- #content-wrap -->
<!-- Ferme la balise content-wrap -->

<?php do_action('ocean_after_content_wrap'); ?>
<!-- Point d'ancrage pour ajouter du contenu après la balise content-wrap -->

<?php get_footer(); ?>
<!-- Appelle le pied de page de votre thème -->