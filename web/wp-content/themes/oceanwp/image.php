<?php

/**
 * The template for displaying image attachments.
 * Ce modèle est utilisé pour afficher les pièces jointes d'images.
 *
 * En savoir plus : http://codex.wordpress.org/Template_Hierarchy
 *
 * @package OceanWP WordPress theme
 */

get_header(); ?>
<!-- Appelle l'en-tête de votre thème -->

<?php do_action('ocean_before_content_wrap'); ?>
<!-- Point d'ancrage pour ajouter du contenu avant la balise content-wrap -->

<div id="content-wrap" class="container clr">
	<!-- Conteneur principal du contenu -->

	<?php do_action('ocean_before_primary'); ?>
	<!-- Point d'ancrage pour ajouter du contenu avant la balise primary -->

	<div id="primary" class="content-area clr">
		<!-- Balise principale de contenu -->

		<?php do_action('ocean_before_content'); ?>
		<!-- Point d'ancrage pour ajouter du contenu avant la balise content -->

		<div id="content" class="site-content">
			<!-- Contenu principal de la page -->

			<?php do_action('ocean_before_content_inner'); ?>
			<!-- Point d'ancrage pour ajouter du contenu avant la balise content-inner -->

			<?php
			while (have_posts()) :
				the_post();
			?>

				<article <?php post_class('image-attachment'); ?>>
					<!-- Balise d'article avec une classe spécifique pour les pièces jointes d'images -->

					<p><?php echo wp_get_attachment_image(get_the_ID(), 'full'); ?></p>
					<!-- Affiche l'image en taille complète -->

					<div class="entry clr">
						<!-- Div d'entrée pour le contenu de l'article -->

						<?php the_content(); ?>
						<!-- Affiche le contenu de l'article -->

						<?php
						// Si les commentaires sont ouverts ou s'il y a au moins un commentaire, charge le modèle de commentaire
						if (comments_open() || '0' !== get_comments_number()) :
							comments_template();
						endif;
						?>
					</div><!-- .entry -->
					<!-- Ferme la balise d'entrée -->

				</article><!-- #post -->
				<!-- Ferme la balise d'article -->

			<?php endwhile; ?>

			<?php do_action('ocean_after_content_inner'); ?>
			<!-- Point d'ancrage pour ajouter du contenu après la balise content-inner -->

		</div><!-- #content -->
		<!-- Ferme la balise content -->

		<?php do_action('ocean_after_content'); ?>
		<!-- Point d'ancrage pour ajouter du contenu après la balise content -->

	</div><!-- #primary -->
	<!-- Ferme la balise primary -->

	<?php do_action('ocean_after_primary'); ?>
	<!-- Point d'ancrage pour ajouter du contenu après la balise primary -->

</div><!-- #content-wrap -->
<!-- Ferme la balise content-wrap -->

<?php do_action('ocean_after_content_wrap'); ?>
<!-- Point d'ancrage pour ajouter du contenu après la balise content-wrap -->

<?php get_footer(); ?>
<!-- Appelle le pied de page de votre thème -->