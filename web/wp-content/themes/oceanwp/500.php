<?php

/**
 * Le modèle pour afficher les pages 500 dans PWA (Application Web Progressive).
 *
 * @package Thème WordPress OceanWP
 * @since   1.8.8
 */

// Inclut l'en-tête spécifique pour les PWA.
pwa_get_header('pwa');

// Déclenche l'action pour gérer les erreurs de serveur.
do_action('ocean_do_server_error');

// Inclut le pied de page spécifique pour les PWA.
pwa_get_footer('pwa');
