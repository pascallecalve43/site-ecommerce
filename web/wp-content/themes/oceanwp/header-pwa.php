<?php

/**
 * The Header for our theme in PWA.
 * Ce fichier est l'en-tête de notre thème pour une application web progressive (PWA).
 *
 * @package OceanWP WordPress theme
 * @since   1.8.8
 */

?>
<!DOCTYPE html>
<!-- Déclaration du type de document -->

<html class="<?php echo esc_attr(oceanwp_html_classes()); ?>" <?php language_attributes(); ?>>
<!-- Balise d'ouverture de la balise html avec des classes et attributs de langue dynamiques -->

<head>
	<!-- Balise d'en-tête -->
	<meta charset="<?php bloginfo('charset'); ?>">
	<!-- Définit l'encodage des caractères en fonction des paramètres du thème -->

	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- Liens vers le profil XFN (XHTML Friends Network) -->

	<?php wp_head(); ?>
	<!-- Appelle les scripts, les styles et autres éléments de l'en-tête WordPress -->

</head>

<body <?php body_class(); ?> <?php oceanwp_schema_markup('html'); ?>>
	<!-- Balise body avec des classes de contenu dynamiques et des attributs de schéma -->